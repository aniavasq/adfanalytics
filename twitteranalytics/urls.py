from django.conf.urls import url

from . import views

urlpatterns = [
    # url(r'^$', views.index, name='index'),    
    url(r'^data/$', views.data, name='data'),
    #url(r'^tag/(?P<tag_name>\w{0,50})/$', views.open_json_stream),
    url(r'^tag/(?P<tag_name>[\w|\W]+)/$', views.open_json_stream),
    url(r'^userdata/$', views.userdata, name='user_data'),
    url(r'^username/(?P<username>\w{0,70})/$', views.open_userjson_stream),
]