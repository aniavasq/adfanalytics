from django.shortcuts import render, redirect, render_to_response
from django.http import StreamingHttpResponse
from django.template import loader, Context
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.contrib.auth.views import update_session_auth_hash
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from pymongo import MongoClient
from .forms import TagForm, TwUserForm#, UserForm, PasswordChangeForm, UserCreationForm#, UserProfileForm
from . import twitter_api_connector as tw
from datetime import datetime
from json import loads as json_loads

todict = tw.todict
client = MongoClient()
db = client.snetanalyticsTW

POST_TAG_JSON_FORMAT = '{\
"tag":{"media_analysed":"%d",\
"total_fav":"%d",\
"total_rt":"%d",\
"total_replies":"%d",\
"total_scope":"%d",\
"total_engagement":"%f"\
},\
"post":{"post_id":"%s",\
"post_fav_count":"%d",\
"post_rt_count":"%d",\
"post_link":"%s",\
"post_thumbnail_url":"%s",\
"post_user":{"username":"%s","followed_by":"%d"},\
"post_engagement":"%f",\
"post_date":"%s"}\
}\n'
LAST_DAY = 0

POST_USER_JSON_FORMAT = '{\
"user":{"media_analysed":"%d",\
"total_fav":"%d",\
"total_rt":"%d",\
"total_replies":"%d",\
"total_scope":"%d",\
"total_friends":"%d",\
"total_organic_engagement":"%f",\
"total_engagement":"%f",\
"oa_organic_engagement":"%f",\
"oa_engagement":"%f"\
},\
"post":{"post_id":"%s",\
"post_fav_count":"%d",\
"post_rt_count":"%d",\
"post_link":"%s",\
"post_thumbnail_url":"%s",\
"post_organic_engagement":"%f",\
"post_engagement":"%f",\
"post_date":"%s"}\
}\n'
LAST_DAY = 0

if settings.SERVER_DEBUG:
    ANALYTICS = '' # DEBUG CONFIG
else:
    ANALYTICS = '/analytics' # PRODUCTION CONFIG
N = 500

@login_required(login_url='%s/admin/login/'%ANALYTICS)
def data(request):
    # print 'ACCES_TOKEN', request.session['access_token']
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # print 'ACCES_TOKEN', request.session['access_token']
        form = TagForm(request.POST)
        tags_cursor = tw.get_tweets_by_hashtag_as_cursor(form.data['tag_name'])
        return StreamingHttpResponse(gen_rendered(form.data['tag_name'], tags_cursor))
    # if a GET (or any other method) we'll create a blank form
    else:
        form = TagForm()

    return render(request, 'twitteranalytics/data.html', {'form': form, 'path': ANALYTICS})

# @csrf_exempt
@login_required(login_url='%s/admin/login/'%ANALYTICS)
def open_json_stream(request, tag_name=None):
    global LAST_DAY
    tweets = None
    if request.method == "POST":
        if not tag_name:
            tag_name = request.GET.get('tag_name', '')
        try:
            range_date = json_loads(request.body)
            date_from = datetime.strptime(range_date['from'], '%Y-%m-%d').date() #range_date['from']
            date_to = datetime.strptime(range_date['to'], '%Y-%m-%d').date() #range_date['to']
        except Exception, e:
            date_from = ''; date_to = ''
            print e
        tweets_cursor = tw.get_tweets_by_hashtag_as_cursor(tag_name)

        #######################
        now_DAY = datetime.now().date().day
        #######################
        if LAST_DAY != now_DAY:
            db.media.remove({"tags.name": tag_name})
            tweets_cursor = tw.get_tweets_by_hashtag_as_cursor(tag_name)
            LAST_DAY = now_DAY
        else:
            tweets = db.media.find({"tags.name": tag_name})
            #next_ = None
            if tweets.count() == 0:
                print "glitch"
                db.media.remove({"tags.name": tag_name})
                tweets_cursor = tw.get_tweets_by_hashtag_as_cursor(tag_name)
        # tw.stream_tweets_by_hashtag("#basketball")
        response = StreamingHttpResponse(gen_rendered(tag_name, tweets_cursor, date_from, date_to, tweets), content_type="text/event-stream")
        return response

def mean(l):
    n = len(l)
    return sum(l) * 1.0 / n if n > 0 else float('nan')

def date_in_range(date_val, date_from, date_to):
    if date_from == '' or date_to == '':
        return True
    return date_from <= date_val <= date_to

def gen_rendered(tag_name, tweets_cursor, date_from='', date_to='', tweets=None):
    rts = [0]; favs = [0]; replies = [0]; scope = [0]; post_count = [0]; engagement = [0]
    range_date = [date_from, date_to]
    posts_engagement = []
    users = {}

    def formatted_post(tweet):
        if tweet['in_reply_to_status_id_str']:
            try:
                tmp_tweet = tw.api.get_status(tweet['in_reply_to_status_id_str'])
                tmp_tweet = todict(tmp_tweet)
                replies[0] += 1
                formatted_post(tmp_tweet)
            except Exception, e:
                print e
                pass
        user_id = tweet['author']['id']
        tag_dict = {'name': tag_name}
        followed_by = 0
        if not user_id in users.keys():
            try:
                user_ = todict(tweet['author'])
            except:
                user_ = {'id': user_id, 'name': '', 'friends_count': 0}
            users[user_id] = user_
        else:
            user_ = users[user_id]
        tweet['author'] = user_

        if date_in_range(tweet['created_at'].date(), range_date[0], range_date[1]):
            post_count[0] += 1
            followed_by = user_['friends_count'] #
            scope[0] += followed_by #
            rts[0] += tweet['retweet_count'] #
            favs[0] += tweet['favorite_count'] #
        if followed_by == 0:
            div = 100
        else:
            div = followed_by
        post_engagement = ( tweet['favorite_count'] + ( 2.0 * tweet['retweet_count'] ) ) / div
        ############################################################################

        if date_in_range(tweet['created_at'].date(), range_date[0], range_date[1]):
            posts_engagement.append( post_engagement )

        engagement[0] = mean( posts_engagement )

        # IMAGE: vier.entities['media']['media_url_https'] if 'media' in vier.entities.keys()
        media_url = static('images/hyperlink_icon.png')
        if 'media' in tweet['entities'].keys():
            if type(tweet['entities']['media']) == list:
                media_url = tweet['entities']['media'][0]['media_url_https']
            else:
                media_url = tweet['entities']['media']['media_url_https']

        try:
            tweet['_id'] = tweet['id']
            db.media.insert_one(tweet)
        except Exception, e:
            print(e)
            pass
        return POST_TAG_JSON_FORMAT%(post_count[0],
            favs[0],
            rts[0],
            replies[0],
            scope[0],
            engagement[0],
            tweet['id'],
            tweet['favorite_count'],
            tweet['retweet_count'],
            'https://twitter.com/%s/status/%s'%(tweet['author']['id'], tweet['id']),
            media_url,
            tweet['author']['name'].replace('\\','\\\\').replace('"','\\"').replace('\n',' '),
            followed_by,
            post_engagement,
            tweet['created_at'])

    ###############################
    if tweets_cursor != None:
        for tweet in tweets_cursor.items(N):
            tweet = todict(tweet)
            tmp_tweet = formatted_post(tweet)
            if date_in_range(tweet['created_at'].date(), range_date[0], range_date[1]):
                yield tmp_tweet
    ###############################
    if tweets != None:
        tweets = todict(tweets)
        for tweet in tweets:
            tmp_tweet = formatted_post(tweet)
            if date_in_range(tweet['created_at'].date(), range_date[0], range_date[1]):
                yield tmp_tweet

@login_required(login_url='%s/admin/login/'%ANALYTICS)
def userdata(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = TwUserForm(request.POST)
        username = form.data['username']
        user_, tweets_cursor = tw.get_tweets_by_user_as_cursor(username)
        return StreamingHttpResponse(gen_rendered_usermedia(username, tweets_cursor, date_from, date_to), content_type="text/event-stream")
    # if a GET (or any other method) we'll create a blank form
    else:
        form = TwUserForm()

    return render(request, 'twitteranalytics/userdata.html', {'form': form, 'path': ANALYTICS})

# @csrf_exempt
@login_required(login_url='%s/admin/login/'%ANALYTICS)
def open_userjson_stream(request, username=None):
    global LAST_DAY
    tweets = None
    if request.method == "POST":
        if not username:
            username = request.GET.get('username', '')
        try:
            range_date = json_loads(request.body)
            date_from = datetime.strptime(range_date['from'], '%Y-%m-%d').date()
            date_to = datetime.strptime(range_date['to'], '%Y-%m-%d').date()
        except Exception, e:
            date_from = ''; date_to = ''
            print e
        user_, tweets_cursor = tw.get_tweets_by_user_as_cursor(username)

        #######################
        now_DAY = datetime.now().date().day
        #######################
        if LAST_DAY != now_DAY:
            db.media.remove({"twusers.name": username})
            user_, tweets_cursor = tw.get_tweets_by_user_as_cursor(username)
            LAST_DAY = now_DAY
        else:
            tweets = db.media.find({"twusers.name": username})
            user_, _ = tw.get_tweets_by_user_as_cursor(username)
            if tweets.count() == 0:
                print "glitch"
                db.media.remove({"twusers.name": username})
                user_, tweets_cursor = tw.get_tweets_by_user_as_cursor(username)

        response = StreamingHttpResponse(gen_rendered_usermedia(user_, tweets_cursor, date_from, date_to, tweets), content_type="text/event-stream")
        return response

def gen_rendered_usermedia(tw_user, tweets_cursor, date_from, date_to, tweets):
    rts = [0]; favs = [0]; replies = [0]; mentions_count = [0]
    scope = [0]; organic_scope = [0]; post_count = [0]
    engagement = [0]; organic_engagement = [0]; oa_engagement = [0]; oa_organic_engagement = [0]
    range_date = [date_from, date_to]
    div = [0]; div_O = [0]
    posts_engagement = []; posts_organic_engagement = []
    users = {}

    scope[0] = tw_user.followers_count
    organic_scope[0] = tw_user.friends_count

    if scope[0] == 0:
        div[0] = 100
        div_O[0] = 100
    else:
        div[0] = scope[0]
        div_O[0] = organic_scope[0]

    K = [ div[0] * 1.0 / div_O[0] ]

    def formatted_post(tweet):
        if tweet['in_reply_to_status_id_str']:
            try:
                tmp_tweet = tw.api.get_status(tweet['in_reply_to_status_id_str'])
                tmp_tweet = todict(tmp_tweet)
                formatted_post(tmp_tweet)
                replies[0] += 1
            except Exception, e:
                print e
                pass
        user_id = tweet['author']['id']
        # tag_dict = {'name': tag_name}
        followed_by = 0
        if not user_id in users.keys():
            try:
                user_ = todict(tweet['author'])
            except:
                user_ = {'id': user_id, 'name': '', 'friends_count': 0}
            users[user_id] = user_
            #scope[0] = user_['friends_count']
        else:
            user_ = users[user_id]
        tweet['author'] = user_
        # if not tag_dict in tweet['tags']:
        #     tweet['tags'].append(tag_dict)

        if date_in_range(tweet['created_at'].date(), range_date[0], range_date[1]):
            post_count[0] += 1
            # print post_count
            # followed_by = user_['friends_count'] #
            # scope[0] += followed_by #
            rts[0] += tweet['retweet_count'] #
            favs[0] += tweet['favorite_count'] #
        #print div_O
            
        post_engagement = ( tweet['favorite_count'] + ( 2.0 * tweet['retweet_count'] ) ) / ( div[0] * K[0] )
        post_organic_engagement = ( tweet['favorite_count'] + ( 2.0 * tweet['retweet_count'] ) ) / ( div_O[0] * K[0] )
        ############################################################################
        # post_love_rate = ( tweet['favorite_count'] ) * 1.0 / div
        ############################################################################
        # post_talk_rate = ( tweet['retweet_count'] ) * 1.0 / div

        if date_in_range(tweet['created_at'].date(), range_date[0], range_date[1]):
            posts_engagement.append( post_engagement )
            posts_organic_engagement.append( post_organic_engagement )
            # posts_love_rate.append( post_love_rate )
            # posts_talk_rate.append( post_talk_rate )

        engagement[0] = mean( posts_engagement )
        organic_engagement[0] = mean( posts_organic_engagement )
        oa_engagement[0] = ( favs[0] + ( 2.0 * rts[0] ) + ( 3.0 * replies[0] ) ) / ( div[0] * K[0] )
        oa_organic_engagement[0] = ( favs[0] + ( 2.0 * rts[0] ) + ( 3.0 * replies[0] ) ) / ( div_O[0] * K[0] )

        # love_rate[0] = mean( posts_love_rate )
        # talk_rate[0] = mean( posts_talk_rate )

        # print '%f'%post_talk_rate

        # IMAGE: vier.entities['media']['media_url_https'] if 'media' in vier.entities.keys()
        # media_url = '#'
        media_url = static('images/hyperlink_icon.png')
        if 'media' in tweet['entities'].keys():
            if type(tweet['entities']['media']) == list:
                media_url = tweet['entities']['media'][0]['media_url_https']
            else:
                media_url = tweet['entities']['media']['media_url_https']

        try:
            tweet['_id'] = tweet['id']
            db.media.insert_one(tweet)
        except Exception, e:
            print(e)
            pass
        """try:
            tag.media_count
        except Exception, e:
            tag.media_count = 0
            print(e)"""
        return POST_USER_JSON_FORMAT%(post_count[0],
            favs[0],
            rts[0],
            replies[0],
            scope[0],
            organic_scope[0],
            organic_engagement[0],
            engagement[0],
            oa_organic_engagement[0],
            oa_engagement[0],
            tweet['id'],
            tweet['favorite_count'],
            tweet['retweet_count'],
            'https://twitter.com/%s/status/%s'%(tweet['author']['id'], tweet['id']),
            media_url,
            post_organic_engagement,
            post_engagement,
            tweet['created_at'])

    ###############################
    if tweets_cursor != None:
        for tweet in tweets_cursor.items(N):
            tweet = todict(tweet)
            tmp_tweet = formatted_post(tweet)
            if date_in_range(tweet['created_at'].date(), range_date[0], range_date[1]):
                yield tmp_tweet

    ###############################
    if tweets != None:
        tweets = todict(tweets)
        for tweet in tweets:
            tmp_tweet = formatted_post(tweet)
            if date_in_range(tweet['created_at'].date(), range_date[0], range_date[1]):
                yield tmp_tweet

    """###############################
        mentions = tw.api.mentions_timeline(count=N)
        mentions_count[0] = len(mentions)
        print "Mentions COUNT", mentions_count
        for mention in mentions:
            mention = todict(mention)
            tmp_tweet = formatted_post(mention)
            if date_in_range(mention['created_at'].date(), range_date[0], range_date[1]):
                    yield tmp_tweet"""
                            