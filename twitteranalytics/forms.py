from django import forms

class TagForm(forms.Form):
    tag_name = forms.CharField(label='Hashtag #', max_length=100)

class TwUserForm(forms.Form):
	username = forms.CharField(label='Username', max_length=100)