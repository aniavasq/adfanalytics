from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.perform_login, name='login'),
    url(r'^login/gram/$', views.instagram_login, name='login'),
    url(r'^login/gram/r/$', views.instagram_request, name='login'),
    url(r'^logout/$', views.perform_logout, name='logout'),
    url(r'^users/$', views.accounts_admin, name='users'),
    url(r'^account/$', views.account_settings, name='account'),
    url(r'^edit/(?P<username>\w{0,50})/$', views.accounts_edit, name='edit'),
    url(r'^password/$', views.update_password, name='password'),
    url(r'^register/$', views.create_user, name='register'),
]