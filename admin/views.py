from django.shortcuts import render, redirect, render_to_response
from django.template import loader, Context
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.contrib.auth.views import update_session_auth_hash
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from .forms import UserForm, PasswordChangeForm, UserCreationForm#, UserProfileForm
from datetime import datetime
from instagram.client import InstagramAPI
from json import loads as json_loads
import urllib

if settings.SERVER_DEBUG:
    ANALYTICS = '' # DEBUG CONFIG
    # REDIRECT_URI = 'http://localhost'
    REDIRECT_URI = 'http://localhost:8000/admin/login/gram'
else:
    ANALYTICS = '/analytics' # PRODUCTION CONFIG
    # http://srblankenbaker.com/analytics/admin/login/gram
    REDIRECT_URI = 'http://srblankenbaker.com/analytics/analytics/admin/login/gram'

CLIENT_ID = '95110fed09ed4691958106cd7e8ea175'
CLIENT_SECRET = 'fff49e2925b24ee0944b56574ed18980'

#https://www.instagram.com/accounts/login/?force_classic_login=&next=/oauth/authorize%3Fscope%3Dbasic%26redirect_uri%3Dhttp%3A//localhost%26response_type%3Dcode%26client_id%3D95110fed09ed4691958106cd7e8ea175

def perform_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                next = request.GET.get('next')
                #print(request.GET)
                if next is not None:
                    return redirect('%s%s'%(ANALYTICS, next))
                return redirect('%s/'%ANALYTICS)
            else:
                return render(request, 'admin/login.html', {'error': 'Disabled account', 'path': ANALYTICS})
        return render(request, 'admin/login.html', {'error': 'Invalid login', 'path': ANALYTICS})
    else:
        next = request.GET['next']
        return render(request, 'admin/login.html', {'path': ANALYTICS, 'next': next})

def instagram_request(request):
    # api = InstagramAPI(client_id=CLIENT_ID, client_secret=CLIENT_SECRET, redirect_uri=REDIRECT_URI)
    # redirect_uri = api.get_authorize_login_url(scope=["basic", "public_content"])
    redirect_uri = "https://api.instagram.com/oauth/authorize/?client_id=%s&redirect_uri=%s&response_type=code&scope=basic+public_content"%(CLIENT_ID, REDIRECT_URI)
    #print redirect_uri
    return redirect(redirect_uri)

def instagram_login(request):
    if request.method == 'GET':
        user = authenticate(username='instagram', password='1n5t4gr4m')
        api = InstagramAPI(client_id=CLIENT_ID, client_secret=CLIENT_SECRET, redirect_uri=REDIRECT_URI)
        #print api.client_id
        gram_code = request.GET.get('code')

        result = exchange_code_for_access_token(gram_code)
        if 'error_type' in result.keys():
            return render(request, 'admin/login.html', {'error': result['error_message'], 'path': ANALYTICS})

        access_token, gram_user = result['access_token'], result['user']
        if user is not None and access_token is not None:
            request.session['access_token'] = access_token
            print request.session['access_token']

            if user.is_active:
                login(request, user)
                return redirect('%s/'%ANALYTICS)
            else:
                return render(request, 'admin/login.html', {'error': 'Can not access', 'path': ANALYTICS})
        else:
            return render(request, 'admin/login.html', {'error': 'Can not access', 'path': ANALYTICS})

def exchange_code_for_access_token(code):
    URL = 'https://api.instagram.com/oauth/access_token'
    values = { 'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'grant_type': 'authorization_code',
        'redirect_uri': REDIRECT_URI,
        'code': code }
    data = urllib.urlencode(values)
    f = urllib.urlopen(URL, data)
    tmp = f.read()
    return json_loads(tmp)

def perform_logout(request):
    logout(request)
    return render(request, 'admin/login.html', {'success': 'Logout successfully', 'path': ANALYTICS})

@login_required(login_url='%s/admin/login/'%ANALYTICS)
@user_passes_test(lambda u: u.is_superuser, login_url='%s/'%ANALYTICS)
def accounts_admin(request, success=False):
    users = User.objects.all()
    return render(request, 'admin/users.html', {'users': users, 'path': ANALYTICS, 'success': success})

@login_required(login_url='%s/admin/login/'%ANALYTICS)
def account_settings(request):
    form_error = ''
    success = False
    if request.method == 'POST':
        data = request.POST.copy()
        data['is_superuser'] = request.user.is_superuser
        uf = UserForm(data, instance=request.user)
        if uf.is_valid():
            uf.save()
            success = 'Saved successfully'
        else:
            form_error = 'User not valid'
    else:
        uf = UserForm(instance=request.user)
    return render(request, 'admin/user_form.html', {'userform':uf, 'form_error': form_error, 'success': success, 'path': ANALYTICS})

@login_required
def update_password(request):
    form = PasswordChangeForm(user=request.user)

    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)

    return render(request, 'admin/user_password.html', { 'form': form, 'path': ANALYTICS})

@login_required(login_url='%s/admin/login/'%ANALYTICS)
@user_passes_test(lambda u: u.is_superuser, login_url='%s/'%ANALYTICS)
def create_user(request):
    form_error = ''
    if request.method == 'POST':
        data = request.POST.copy()
        data['date_joined'] = datetime.now()
        data['password'] = data['password1']
        uf = UserCreationForm(data)
        uf.date_joined = datetime.now()
        if uf.is_valid():
            uf_data = uf.cleaned_data
            user = User.objects.create_user(uf_data['username'],
                uf_data['email'], 
                uf_data['password1'],
                first_name=uf_data['first_name'],
                last_name=uf_data['last_name'])
            return accounts_admin(request, success='User created successfully')
        else:
            form_error = 'Form is not valid'
    else:
        uf = UserCreationForm()
    return render(request, 'admin/user_create.html', {'userform':uf, 'form_error': form_error, 'path': ANALYTICS})

@login_required(login_url='%s/admin/login/'%ANALYTICS)
@user_passes_test(lambda u: u.is_superuser, login_url='%s/'%ANALYTICS)
def accounts_edit(request, username=None):
    form_error = ''
    success = False
    if request.method == 'POST':
        data = request.POST.copy()
        user = User.objects.get(username=data['username'])
        if user:
            user.first_name = data['first_name']
            user.last_name = data['last_name']
            user.email = data['email']
            user.is_active = ( 'is_active' in data.keys() )
            user.is_superuser = ( 'is_superuser' in data.keys() )
            user.save()
            success = 'Saved successfully'
            return accounts_admin(request, success='Changes saved successfully')
        else:
            form_error = 'User not valid'
    else:
        user = User.objects.get(username=username)
        print user.__dict__
        uf = UserForm(instance=user)
    return render(request, 'admin/user_edit.html', {'userform':uf, 'form_error': form_error, 'path': ANALYTICS})