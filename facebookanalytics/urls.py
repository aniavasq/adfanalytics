from django.conf.urls import url

from . import views

urlpatterns = [
    # url(r'^$', views.index, name='index'),    
    url(r'^data/$', views.data, name='data'),
    url(r'^page/(?P<page_name>\w{0,50})/$', views.open_json_stream),
]