from django import forms

class PageForm(forms.Form):
    page_name = forms.CharField(label='Page Name', max_length=100)