from django.shortcuts import render, redirect, render_to_response
from django.http import StreamingHttpResponse
from django.template import loader, Context
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.contrib.auth.views import update_session_auth_hash
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from pymongo import MongoClient
from .forms import PageForm
from . import fb_api_connector as fb
from datetime import datetime
from json import loads as json_loads
# from instagram.client import Tag
# from instagram.client import User as gram_User

client = MongoClient()
db = client.snetanalyticsFb

JSON_FORMAT = '{\
"tag":{"media_analysed":"%d",\
"total_likes":"%d",\
"total_comments":"%d",\
"total_shares":"%d",\
"total_scope":"%d",\
"total_engagement":"%f"\
},\
"post":{"post_id":"%s",\
"post_like_count":"%d",\
"post_comment_count":"%d",\
"post_share_count":"%d",\
"post_link":"%s",\
"post_thumbnail_url":"%s",\
"post_engagement":"%f",\
"post_date":"%s"}\
}\n'
LAST_DAY = 0

if settings.SERVER_DEBUG:
    ANALYTICS = '' # DEBUG CONFIG
else:
    ANALYTICS = '/analytics' # PRODUCTION CONFIG

"""@login_required(login_url='%s/admin/login/'%ANALYTICS)
def index(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = PageForm(request.POST)
        tag = gram.get_tag(form.data['tag_name'])
        posts, next_ = gram.get_recent_media_by_tag(tag)
        return StreamingHttpResponse(gen_rendered(tag, posts, next_))
    # if a GET (or any other method) we'll create a blank form
    else:
        form = PageForm()

    return render(request, 'facebookanalytics/index.html', {'form': form, 'path': ANALYTICS})
    # return render(request, 'data.html', {'form': form, 'path': ANALYTICS})"""

@login_required(login_url='%s/admin/login/'%ANALYTICS)
def data(request):
    # print(request.user.is_superuser)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = PageForm(request.POST)
        page = fb.get_page_by_name(form.data['tag_name'])
        posts, next_ = get_posts_with_next(page['posts'])
        #tag = gram.get_tag(form.data['tag_name'])
        #posts, next_ = gram.get_recent_media_by_tag(tag)
        return StreamingHttpResponse(gen_rendered(posts, next_))
    # if a GET (or any other method) we'll create a blank form
    else:
        form = PageForm()

    return render(request, 'facebookanalytics/data.html', {'form': form, 'path': ANALYTICS})

# @csrf_exempt
def open_json_stream(request, page_name=None):
    global LAST_DAY
    if request.method == "POST":
        if not page_name:
            page_name = request.GET.get('tag_name', '')
        try:
            range_date = json_loads(request.body)
            date_from = datetime.strptime(range_date['from'], '%Y-%m-%d').date() #range_date['from']
            date_to = datetime.strptime(range_date['to'], '%Y-%m-%d').date() #range_date['to']
        except Exception, e:
            date_from = ''; date_to = ''
            print e
        if date_to == '':
            date_to = datetime.now().date().strftime('%Y-%m-%d')
        page = fb.get_page_by_name(page_name)

        #######################
        now_DAY = datetime.now().date().day
        #######################
        if date_from == '' or date_to == '':
            posts, next_ = fb.get_posts_with_next(page['posts'])
        else:
            posts, next_ = fb.get_posts_by_page_range(page, date_from, date_to)

        """if LAST_DAY != now_DAY:
                                    db.media.remove({"page.name": page_name})
                                    posts, next_ = get_posts_with_next(page['posts'])
                                    LAST_DAY = now_DAY
                                else:
                                    posts = db.media.find({"page.name": page_name})
                                    next_ = None
                                    if posts.count() == 0:
                                        print "glitch"
                                        db.media.remove({"page.name": page_name})
                                        posts, next_ = get_posts_with_next(page['posts'])"""

        response = StreamingHttpResponse(gen_rendered(page, posts, next_, date_from, date_to), content_type="text/event-stream")
        return response

def mean(l):
    n = len(l)
    return sum(l) * 1.0 / n if n > 0 else float('nan')

def gen_rendered(page, posts, next_, date_from, date_to):
    shares = [0]; comments = [0]; likes = [0]; post_count = [0]; engagement = [0]
    posts_engagement = []
    # print(page)
    def formatted_post(post):
        post_data = fb.get_post_data_by_id(post['id'])
        post_like_count = post_data['likes']['summary']['total_count']
        post_comment_count = post_data['comments']['summary']['total_count']
        #print(post_data)
        post_thumbnail_url = ''; post_link = ''; post_share_count = 0
        if ('attachments' in post_data.keys()):
            data = post_data['attachments']['data']
            if type(data) == list:
                if 'media' in data[0].keys():
                    post_thumbnail_url = data[0]['media']['image']['src']
                elif 'subattachments' in data[0].keys() and type(data[0]['subattachments']['data']) == list and 'media' in data[0]['subattachments']['data'][0].keys():
                    print(post_data['id'])
                    post_thumbnail_url = data[0]['subattachments']['data'][0]['media']['image']['src']
                elif 'subattachments' in data[0].keys() and 'media' in data[0]['subattachments']['data'].keys():
                    print(post_data['id'])
                    post_thumbnail_url = data[0]['subattachments']['data']['media']['image']['src']
                else:
                    post_thumbnail_url = static('images/hyperlink_icon.png')
                    # print(data[0])
            else:
                if 'media' in data.keys():
                    post_thumbnail_url = data['media']['image']['src']
                elif 'subattachments' in data.keys() and type(data['subattachments']['data']) == list and 'media' in data['subattachments']['data'][0].keys():
                    print(post_data['id'])
                    post_thumbnail_url = data['subattachments'][0]['media']['image']['src']
                elif 'subattachments' in data.keys() and 'media' in data['subattachments']['data'].keys():
                    print(post_data['id'])
                    post_thumbnail_url = data['subattachments']['data']['media']['image']['src']
                else:
                    post_thumbnail_url = static('images/hyperlink_icon.png')
                    # print(data)
        if ('shares' in post_data.keys()):
            try:
                post_share_count = post_data['shares']['count']
            except Exception, e:
                print(e)
        div = page[u'fan_count']*1.568
        ################################################################################################
        if div == 0: div = 100
        post_engagement = (post_like_count * 1. + post_comment_count * 2. + post_share_count * 3.) / div
        posts_engagement.append(post_engagement)
        shares[0] += post_share_count
        comments[0] += post_comment_count
        likes[0] += post_like_count
        engagement[0] = mean(posts_engagement)
        post_count[0] += 1
        ################################################################################################
        return JSON_FORMAT%(post_count[0],
            likes[0],
            comments[0],
            shares[0],
            div,
            engagement[0], ###
            post_data['id'],
            post_like_count,
            post_comment_count,
            post_share_count,
            "http://www.facebook.com/" + post_data['id'],
            post_thumbnail_url,
            post_engagement,
            post_data['created_time'])

    ###############################
    # posts = todict(posts)

    for post in posts:
        tmp_post = formatted_post(post)
        yield tmp_post
        #if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
        #    yield tmp_post
    # page_next_posts = get_from_next_url(next_)
    # posts, next_ = fb.get_posts_with_next(page_next_posts)
    if date_from != '' and date_to != '':
        while next_:
            page_next_posts = fb.get_from_next_url(next_)
            more_posts, next_ = fb.get_posts_with_next(page_next_posts)
            for post in more_posts:
                tmp_post = formatted_post(post)
                yield tmp_post
            # if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
            #    yield tmp_post