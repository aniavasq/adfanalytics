import facebook
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
import json
from json import load as json_load
try:
    from json import loads as json_loads
except ImportError:
    json_loads = json_load

ACCES_TOKEN = '243439722681475|U3P2y7jrSx4tD6nQN-ycJMT2biA'

graph = facebook.GraphAPI(access_token=ACCES_TOKEN)

graph_search = lambda q, object_type: graph.request('search', {'q': q, 'type': object_type})
graph_get_object = graph.get_object

def get_from_next_url(next_url):
    return json_load(urllib2.urlopen(next_url))

def get_user_by_username(username):
    tmp = graph_search(username, 'user')
    try:
        user_id = tmp['data'][0]['id']
        return get_userdata_by_id(user_id)
    except:
        return {'username': username}

def get_user_by_id(user_id):
    return graph_get_object(user_id)

########################################################################################################
########################################################################################################

def get_page_by_name(page_name):
    tmp = graph_search(page_name, 'page')
    try:
        page_id = tmp['data'][0]['id']
        return get_page_by_id(page_id)
    except:
        return {'name': page_name}

def get_page_by_id(page_id):
    return graph_get_object(page_id, metadata=1, fields='posts.limit(100),likes,engagement,fan_count,metadata{type}')

def get_posts_with_next(posts):
    try:
        next_ = posts['paging']['next']
    except:
        next_ = None

    try:
        data = posts['data']
    except:
        data = None

    return data, next_

def get_posts_by_page_id(page_id):
    page = get_page_by_id(page_id)
    return get_posts_by_page(page)

def get_posts_by_page(page):
    all_posts = []

    posts, next_ = get_posts_with_next(page['posts'])
    for post in posts:
        all_posts.append(get_post_data_by_id(post['id']))

    while next_:
        page_next_posts = get_from_next_url(next_)
        posts, next_ = get_posts_with_next(page_next_posts)
        for post in posts:
            all_posts.append(get_post_data_by_id(post['id']))
    return all_posts

def get_post_data_by_id(post_id):
    return graph_get_object(post_id, fields='likes.limit(1).summary(true),comments.limit(1).summary(true),shares,attachments,created_time')

def get_posts_by_page_range(page, since, until):
    posts = graph_get_object('/'+page['id']+'/posts', since=since, until=until)
    return get_posts_with_next(posts)

# graph_get_object(tmp['posts']['data'][0]['id'], fields='likes.limit(1).summary(true),comments.limit(1).summary(true),shares')
# graph_get_object(tmp['posts']['data'][0]['id'], fields='likes.limit(1).summary(true),comments.limit(1).summary(true),shares,attachments')
# graph_get_object(tmp['posts']['data'][0]['id'], fields='likes.limit(1).summary(true),comments.limit(1).summary(true),shares,attachments,created_time')
# graph.get_object('/'+page['id']+'/posts', since='2013-01-01', until='2014-01-10')
