"""snetanalytics URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.views.generic.base import TemplateView
from django.conf import settings
from admin import views

class HomePageView(TemplateView):

    def get_path(self):
        if settings.SERVER_DEBUG:
            ANALYTICS = '' # DEBUG CONFIG
        else:
            ANALYTICS = '/analytics' # PRODUCTION CONFIG
        return ANALYTICS

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(HomePageView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['path'] = self.get_path()
        return context
# from django.contrib import admin

urlpatterns = [
    url(r'^$', HomePageView.as_view(template_name='index.html'), name='index'),
    url(r'^gram/', include('instagramanalytics.urls')),
    url(r'^fb/', include('facebookanalytics.urls')),
    url(r'^tw/', include('twitteranalytics.urls')),
    url(r'^admin/', include('admin.urls')),
    url(r'^analytics/admin/login/gram/$', views.instagram_login, name='login'),
    # url(r'^admin/', admin.site.urls),
]
