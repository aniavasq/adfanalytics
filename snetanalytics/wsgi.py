"""
WSGI config for snetanalytics project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os, sys, site

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "snetanalytics.settings")

try:
    # Add the site packages, to override any system-wide packages
    site.addsitedir('/home/blankenbaker/webapps/adfanalytics/adfanalytics/venv/lib/python2.7/site-packages')
    
    # Activate the virtualenv
    activate_this = os.path.expanduser(os.path.abspath('/home/blankenbaker/webapps/adfanalytics/adfanalytics/venv/bin/activate_this.py'))
    execfile(activate_this, dict(__file__=activate_this))
except Exception as e:
    print e

application = get_wsgi_application()
