from django import forms
from django.contrib.auth.models import User#, UserProfile
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm, UserCreationForm

class TagForm(forms.Form):
    tag_name = forms.CharField(label='Hashtag #', max_length=100)

class GramUserForm(forms.Form):
	username = forms.CharField(label='Username', max_length=100)