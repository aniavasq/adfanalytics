from django.shortcuts import render, redirect, render_to_response
from django.http import StreamingHttpResponse
from django.template import loader, Context
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.contrib.auth.views import update_session_auth_hash
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from pymongo import MongoClient
from .forms import TagForm, GramUserForm#, UserForm, PasswordChangeForm, UserCreationForm#, UserProfileForm
from . import gram_api_connector as gram
from datetime import datetime
from json import loads as json_loads
from instagram.client import InstagramAPI, Tag
from instagram.client import User as gram_User

client = MongoClient()
todict = gram.todict
db = client.snetanalyticsGram

POST_TAG_JSON_FORMAT = '{\
"tag":{"media_count":"%d",\
"media_analysed":"%d",\
"total_likes":"%d",\
"total_comments":"%d",\
"total_scope":"%d",\
"total_engagement":"%f",\
"total_love_rate":"%f",\
"total_talk_rate":"%f"\
},\
"post":{"post_id":"%s",\
"post_like_count":"%d",\
"post_comment_count":"%d",\
"post_link":"%s",\
"post_thumbnail_url":"%s",\
"post_user":{"username":"%s","full_name":"%s","followed_by":"%d"},\
"post_engagement":"%f",\
"post_love_rate":"%f",\
"post_talk_rate":"%f",\
"post_date":"%s"}\
}\n'
LAST_DAY = 0

POST_USER_JSON_FORMAT = '{\
"user":{"media_count":"%d",\
"media_analysed":"%d",\
"total_likes":"%d",\
"total_comments":"%d",\
"total_scope":"%d",\
"total_engagement":"%f",\
"total_love_rate":"%f",\
"total_talk_rate":"%f"\
},\
"post":{"post_id":"%s",\
"post_like_count":"%d",\
"post_comment_count":"%d",\
"post_link":"%s",\
"post_thumbnail_url":"%s",\
"post_engagement":"%f",\
"post_love_rate":"%f",\
"post_talk_rate":"%f",\
"post_date":"%s"}\
}\n'
LAST_DAY = 0

if settings.SERVER_DEBUG:
    ANALYTICS = '' # DEBUG CONFIG
else:
    ANALYTICS = '/analytics' # PRODUCTION CONFIG

@login_required(login_url='%s/admin/login/'%ANALYTICS)
def data(request):
    # print 'ACCES_TOKEN', request.session['access_token']
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # print 'ACCES_TOKEN', request.session['access_token']
        form = TagForm(request.POST)
        tag = gram.get_tag(form.data['tag_name'])
        posts, next_ = gram.get_recent_media_by_tag(tag)
        return StreamingHttpResponse(gen_rendered(tag, posts, next_))
    # if a GET (or any other method) we'll create a blank form
    else:
        form = TagForm()

    return render(request, 'instagramanalytics/data.html', {'form': form, 'path': ANALYTICS})

# @csrf_exempt
@login_required(login_url='%s/admin/login/'%ANALYTICS)
def open_json_stream(request, tag_name=None):
    global LAST_DAY
    if request.method == "POST":
        if not tag_name:
            tag_name = request.GET.get('tag_name', '')
        try:
            range_date = json_loads(request.body)
            date_from = datetime.strptime(range_date['from'], '%Y-%m-%d').date() #range_date['from']
            date_to = datetime.strptime(range_date['to'], '%Y-%m-%d').date() #range_date['to']
        except Exception as e:
            date_from = ''; date_to = ''
            print "open_json_stream", e
        tag = gram.get_tag(tag_name)

        #######################
        now_DAY = datetime.now().date().day
        #######################
        if LAST_DAY != now_DAY:
            db.media.remove({"tags.name": tag_name})
            posts, next_ = gram.get_recent_media_by_tag(tag)
            LAST_DAY = now_DAY
        else:
            posts = db.media.find({"tags.name": tag_name})
            next_ = None
            if posts.count() == 0:
                print "glitch"
                db.media.remove({"tags.name": tag_name})
                posts, next_ = gram.get_recent_media_by_tag(tag)

        response = StreamingHttpResponse(gen_rendered(tag, posts, next_, date_from, date_to), content_type="text/event-stream")
        return response

def mean(l):
    n = len(l)
    return sum(l) * 1.0 / n if n > 0 else float('nan')

def date_in_range(date_val, date_from, date_to):
    if date_from == '' or date_to == '':
        return True
    return date_from <= date_val <= date_to

def gen_rendered(tag, posts, next_, date_from, date_to):
    comments = [0]; likes = [0]; scope = [0]; post_count = [0]; engagement = [0]; love_rate = [0]; talk_rate = [0]
    range_date = [date_from, date_to]
    posts_engagement = []; posts_love_rate = []; posts_talk_rate = []
    users = {}
    gram_get_userdata_by_id = gram.get_userdata_by_id

    def formatted_post(post):
        user_id = post['user']['id']
        tag_dict = {'name': tag.name}
        followed_by = 0
        if not user_id in users.keys():
            try:
                user_ = gram_get_userdata_by_id(user_id)
            except:
                user_ = {'id': user_id, 'username': '', 'full_name': '', 'counts':{'followed_by':0}}
            users[user_id] = user_
        else:
            user_ = users[user_id]
        post['user'] = user_
        if not tag_dict in post['tags']:
            post['tags'].append(tag_dict)

        if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
            post_count[0] += 1
            followed_by = user_['counts']['followed_by'] #
            scope[0] += followed_by #
            comments[0] += post['comment_count'] #
            likes[0] += post['like_count'] #
        if followed_by == 0:
            # div = 1
            div = 100
        else:
            div = followed_by
        post_engagement = ( post['like_count'] + ( 2.0 * post['comment_count'] ) ) / div
        ############################################################################
        post_love_rate = ( post['like_count'] ) * 1.0 / div
        ############################################################################
        post_talk_rate = ( post['comment_count'] ) * 1.0 / div

        if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
            posts_engagement.append( post_engagement )
            posts_love_rate.append( post_love_rate )
            posts_talk_rate.append( post_talk_rate )

        engagement[0] = mean( posts_engagement )
        love_rate[0] = mean( posts_love_rate )
        talk_rate[0] = mean( posts_talk_rate )

        #print '%f'%post_talk_rate

        try:
            post['_id'] = post['id']
            db.media.insert_one(post)
        except Exception as e:
            print "post['_id']", e
            pass
        try:
            tag.media_count
        except Exception as e:
            tag.media_count = 0
            print "tag.media_count", e
        return POST_TAG_JSON_FORMAT%(tag.media_count,
            post_count[0],
            likes[0],
            comments[0],
            scope[0],
            engagement[0],
            love_rate[0],
            talk_rate[0],
            post['id'],
            post['like_count'],
            post['comment_count'],
            post['link'],
            post['images']['thumbnail']['url'],
            post['user']['username'].replace('\\','\\\\').replace('"','\\"').replace('\n',' '),
            post['user']['full_name'].replace('\\','\\\\').replace('"','\\"').replace('\n',' '),
            followed_by,
            post_engagement,
            post_love_rate,
            post_talk_rate,
            post['created_time'])

    ###############################
    posts = todict(posts)
    for post in posts:
        tmp_post = formatted_post(post)
        if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
            yield tmp_post

    while next_:
        more_posts, next_ = gram.get_recent_media_by_next(tag, next_)
        more_posts = todict(more_posts)
        ###############################
        for post in more_posts:
            tmp_post = formatted_post(post)
            if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
                yield tmp_post



@login_required(login_url='%s/admin/login/'%ANALYTICS)
def userdata(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = GramUserForm(request.POST)
        user_ = gram.get_user(form.data['username'])
        posts, next_ = gram.get_recent_media_by_user(user_)
        return StreamingHttpResponse(gen_rendered_usermedia(user_, posts, next_, date_from, date_to), content_type="text/event-stream")
    # if a GET (or any other method) we'll create a blank form
    else:
        form = GramUserForm()

    return render(request, 'instagramanalytics/userdata.html', {'form': form, 'path': ANALYTICS})

@login_required(login_url='%s/admin/login/'%ANALYTICS)
def loggeduserdata(request):
    # if this is a POST request we need to process the form data
    ACCES_TOKEN = request.session['access_token']
    api = InstagramAPI(access_token=ACCES_TOKEN, client_secret=gram.CLIENT_SECRET)
    user_ = api.user()
    if request.method == 'POST':
        # form = GramUserForm(request.POST)
        # user_ = gram.get_user(form.data['username'])
        # posts, next_ = gram.get_recent_media_by_user(user_)
        posts, next_ = api.user_recent_media()
        print posts

        try:
            range_date = json_loads(request.body)
            date_from = datetime.strptime(range_date['from'], '%Y-%m-%d').date() #range_date['from']
            date_to = datetime.strptime(range_date['to'], '%Y-%m-%d').date() #range_date['to']
        except Exception as e:
            date_from = ''; date_to = ''
            print e

        return StreamingHttpResponse(gen_rendered_usermedia(todict(user_), posts, next_, date_from, date_to), content_type="text/event-stream")
    # if a GET (or any other method) we'll create a blank form
    else:
        gramuser = user_

    return render(request, 'instagramanalytics/loggeduserdata.html', {'gramuser': gramuser, 'path': ANALYTICS})

# @csrf_exempt
@login_required(login_url='%s/admin/login/'%ANALYTICS)
def open_userjson_stream(request, username=None):
    global LAST_DAY
    if request.method == "POST":
        if not username:
            username = request.GET.get('username', '')
        try:
            range_date = json_loads(request.body)
            date_from = datetime.strptime(range_date['from'], '%Y-%m-%d').date() #range_date['from']
            date_to = datetime.strptime(range_date['to'], '%Y-%m-%d').date() #range_date['to']
        except Exception as e:
            date_from = ''; date_to = ''
            print e
        user_ = gram.get_user(username)

        #######################
        now_DAY = datetime.now().date().day
        #######################
        if LAST_DAY != now_DAY:
            db.media.remove({"gramusers.username": username})
            posts, next_ = gram.get_recent_media_by_user(user_)
            LAST_DAY = now_DAY
        else:
            posts = db.media.find({"gramusers.username": username})
            next_ = None
            if posts.count() == 0:
                print "glitch"
                db.media.remove({"gramusers.username": username})
                posts, next_ = gram.get_recent_media_by_user(user_)

        response = StreamingHttpResponse(gen_rendered_usermedia(user_, posts, next_, date_from, date_to), content_type="text/event-stream")
        return response

def gen_rendered_usermedia(user_, posts, next_, date_from, date_to):
    comments = [0]; likes = [0]; scope = [0]; post_count = [0]; engagement = [0]; love_rate = [0]; talk_rate = [0]
    range_date = [date_from, date_to]
    posts_engagement = []; posts_love_rate = []; posts_talk_rate = []
    gram_get_userdata_by_id = gram.get_userdata_by_id

    user_id = user_['id']
    try:
        user_data = gram_get_userdata_by_id(user_id)
    except:
        user_data = {'id': user_id, 'username': '', 'full_name': '', 'counts':{'followed_by':0, 'media': 0}}
    # print(user_data)
    scope = [user_data['counts']['followed_by']] #

    def formatted_post(post):
        followed_by = scope[0]
        if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
            post_count[0] += 1
            comments[0] += post['comment_count'] #
            likes[0] += post['like_count'] #
        if followed_by == 0:
            # div = 1
            div = 100
        else:
            div = followed_by
        post_engagement = ( post['like_count'] + ( 2.0 * post['comment_count'] ) ) / div
        ############################################################################
        post_love_rate = ( post['like_count'] ) * 1.0 / div
        ############################################################################
        post_talk_rate = ( post['comment_count'] ) * 1.0 / div

        if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
            posts_engagement.append( post_engagement )
            posts_love_rate.append( post_love_rate )
            posts_talk_rate.append( post_talk_rate )

        engagement[0] = mean( posts_engagement )
        love_rate[0] = mean( posts_love_rate )
        talk_rate[0] = mean( posts_talk_rate )

        try:
            post['_id'] = post['id']
            db.media.insert_one(post)
        except Exception as e:
            print(e)
            pass
        return POST_USER_JSON_FORMAT%(user_data['counts']['media'],
            post_count[0],
            likes[0],
            comments[0],
            scope[0],
            engagement[0],
            love_rate[0],
            talk_rate[0],
            post['id'],
            post['like_count'],
            post['comment_count'],
            post['link'],
            post['images']['thumbnail']['url'],
            post_engagement,
            post_love_rate,
            post_talk_rate,
            post['created_time'])

    ###############################
    posts = todict(posts)
    for post in posts:
        tmp_post = formatted_post(post)
        if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
            yield tmp_post

    while next_:
        more_posts, next_ = gram.get_recent_usermedia_by_next(user_, next_)
        more_posts = todict(more_posts)
        ###############################
        for post in more_posts:
            tmp_post = formatted_post(post)
            if date_in_range(post['created_time'].date(), range_date[0], range_date[1]):
                yield tmp_post