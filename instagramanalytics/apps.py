from django.apps import AppConfig


class InstagramanalyticsConfig(AppConfig):
    name = 'instagramanalytics'
