from instagram.client import InstagramAPI, Tag, User
from itertools import chain
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
import json
from json import load as json_load
try:
    from json import loads as json_loads
except ImportError:
    json_loads = json_load


#ACCES_TOKEN = "3230554059.95110fe.afad051bec9049c2984a887c4afcd399"
# https://api.instagram.com/oauth/authorize/?client_id=95110fed09ed4691958106cd7e8ea175&redirect_uri=http://localhost&response_type=token&scope=public_content
ACCES_TOKEN = "3230554059.cca6435.31b481a979d947eda4548108d1e42a65"
# ACCES_TOKEN = "2136707.4dd19c1.d077b227b0474d80a5665236d2e90fcf"
ACCES_TOKEN_2 = "3230554059.1677ed0.5055721b0b4740caa166f862b61918d1"
ACCES_TOKEN_3 = "3084658140.1677ed0.7521c6afe3e54bdc8fd1710c6baeddc7"
CLIENT_ID = "95110fed09ed4691958106cd7e8ea175"
CLIENT_SECRET = "fff49e2925b24ee0944b56574ed18980"
CLIENT_SECRET_2 = "fff49e2925b24ee0944b56574ed18980"

api = InstagramAPI(access_token=ACCES_TOKEN, client_secret=CLIENT_SECRET)
api_2 = InstagramAPI(access_token=ACCES_TOKEN_2, client_secret=CLIENT_SECRET_2)

api_tag_recent_media = api.tag_recent_media

def todict(obj, classkey=None):
    if isinstance(obj, dict):
        data = {}
        for (k, v) in obj.items():
            data[k] = todict(v, classkey)
        return data
    elif hasattr(obj, "_ast"):
        return todict(obj._ast())
    elif hasattr(obj, "__iter__"):
        return [todict(v, classkey) for v in obj]
    elif hasattr(obj, "__dict__"):
        data = dict([(key, todict(value, classkey)) 
            for key, value in obj.__dict__.iteritems() 
            if not callable(value) and not key.startswith('_')])
        if classkey is not None and hasattr(obj, "__class__"):
            data[classkey] = obj.__class__.__name__
        return data
    else:
        return obj

def get_userdata_by_username(user_name):
    user_ = get_user(user_name)
    return get_userdata_by_id(user_['id'])

def get_userdata_by_id(user_id):
    try:
        return json_load(urllib2.urlopen("https://api.instagram.com/v1/users/%s/?access_token=%s"%(user_id, ACCES_TOKEN)))['data']
    except:
        response = urllib2.urlopen("https://api.instagram.com/v1/users/%s/?access_token=%s"%(user_id, ACCES_TOKEN))
        return json_loads(response.read().decode("utf-8"))['data']

def get_user(user_name):
    user_ = User("",username=user_name)
    try:
        user_ = json.load(urllib2.urlopen("https://api.instagram.com/v1/users/search?q=%s&access_token=%s"%(user_name, ACCES_TOKEN)))['data'][0]
        #user_id = user_['data'][0]['id']
    except: pass
    return user_#user_id

def get_media_by_username(user_name):
    media_posts = []
    user_= get_user(user_name)
    recent_media, next_ = api.user_recent_media(user_id=user_['id'])
    media_posts.extend(recent_media)

    while next_:
        recent_media, next_ = api.user_recent_media(user_id=user_['id'], with_next_url=next_)
        media_posts.extend(recent_media)
    return user_, media_posts

def get_recent_media_by_user(user_):
    try:
        recent_media, next_ = api.user_recent_media(user_id=user_['id'])
    except Exception as e:
        print "get_recent_media_by_user", e
        recent_media, next_ = api_2.user_recent_media(user_id=user_['id'])
    return recent_media, next_

def get_recent_usermedia_by_next(user_, next_):
    try:
        recent_media, next_ = api.user_recent_media(user_id=user_['id'], with_next_url=next_)
    except Exception as e:
        print "get_recent_usermedia_by_next", e
        recent_media, next_ = api_2.user_recent_media(user_id=user_['id'], with_next_url=next_)
    return recent_media, next_

def get_followers_by_username(user_name):
    user_ = get_user(user_name)
    return user_, get_followers_by_user_id(user_['id'])

def get_followers_by_user_id(user_id):
    followers, next_ = api.user_followed_by(user_id)
    while next_:
        more_follows, next_ = api.user_followed_by(with_next_url=next_)
        followers.extend(more_follows)
        return followers
########################################################################################################
########################################################################################################

def get_tag(tag_name):
    tag = Tag(tag_name)
    try:
        # tag = api.tag_search(tag_name)[0][0]
        tag = api.tag(tag_name)
    except: pass
    return tag

def get_media_by_tag(tag_name):
    tag = get_tag(tag_name)

    return tag, get_media_by_tag_name(tag.name)

def get_media_by_tag_name(tag_name):
    recent_media, next_ = api_tag_recent_media(tag_name=tag_name)
    recent_media_extend = recent_media.extend
    #####
    while next_:
        more_recent_media, next_ = api_tag_recent_media(tag_name=tag_name, with_next_url=next_)
        recent_media_extend(more_recent_media)
    return recent_media

def get_recent_media_by_tag(tag):
    import traceback

    try:
        recent_media, next_ = api.tag_recent_media(tag_name=tag.name)
    except Exception as e:
        print "get_recent_media_by_tag", e
        traceback.print_exc()
        recent_media, next_ = api_2.tag_recent_media(tag_name=tag.name)
    return recent_media, next_

def get_recent_media_by_next(tag, next_):
    try:
        recent_media, next_ = api.tag_recent_media(tag_name=tag.name, with_next_url=next_)
    except Exception as e:
        print "get_recent_media_by_next", e
        recent_media, next_ = api_2.tag_recent_media(tag_name=tag.name, with_next_url=next_)
    return recent_media, next_