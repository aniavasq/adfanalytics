from django.conf.urls import url

from . import views

urlpatterns = [
    # url(r'^$', views.index, name='index'),    
    url(r'^data/$', views.data, name='data'),
    url(r'^tag/(?P<tag_name>\w{0,50})/$', views.open_json_stream),
    url(r'^userdata/$', views.userdata, name='user_data'),
    url(r'^loggeduserdata/$', views.loggeduserdata, name='logged_user_data'),
    url(r'^username/(?P<username>\w{0,50})/$', views.open_userjson_stream),
]
"""url(r'^login/$', views.perform_login, name='login'),
            url(r'^logout/$', views.perform_logout, name='logout'),
            url(r'^users/$', views.accounts_admin, name='users'),
            url(r'^account/$', views.account_settings, name='account'),
            url(r'^edit/(?P<username>\w{0,50})/$', views.accounts_edit, name='edit'),
            url(r'^password/$', views.update_password, name='password'),
            url(r'^register/$', views.create_user, name='register'),"""