from autobahn.twisted.websocket import WebSocketServerProtocol, \
    WebSocketServerFactory
from json import loads as json_loads
from json import dumps as json_dumps
from sys import stdout
from twisted.python import log
from twisted.internet import reactor
from gc import collect as gc_collect
from pprint import pprint

DEBUG = True
PORT = 8000
URL = "ws://8.8.8.8:%d/ws"
MAX_CON = 100

"""@package docstring
Websocket interface module.
Websocket interface implementation for prediction model implementation, input
interfaces are: studentID, and a list of courseID, provides the quality and
prediction values after classification/estimation process.
"""

class BackendServerProtocol(WebSocketServerProtocol):
    dispatchers = {}

    def onConnect(self, request):
        print("Client connecting: %s"%( request.peer ))

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary):
    	json_input = ""
        if isBinary:
            print("Binary message received: %d bytes"%( len( payload ) ))
        else:
            json_string = format(payload.decode('utf8'))
            json_input = json_loads( json_string )
        self.sendMessage( json_dumps(json_input), False )
        
    def onClose(self, wasClean, code, reason):     
        gc_collect()
        print("WebSocket connection closed: %s"%( reason ))

if __name__ == '__main__':

    log.startLogging( stdout )

    factory = WebSocketServerFactory(URL%PORT)
    factory.protocol = BackendServerProtocol
    factory.setProtocolOptions(maxConnections=MAX_CON)

    reactor.listenTCP( PORT, factory )
    reactor.run()